Welcome to Libre-SOC's OpenPOWER ISA simulator
=============================================

.. automodule:: openpower.simulator.test_sim.DecoderTestCase
    :members:

.. toctree:: gen/modules
   :maxdepth: 4
   :caption: OpenPOWER ISA Contents:

.. toctree:: 
   :maxdepth: 4
   :caption: OpenPOWER ISA Contents:
   :glob: 



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
